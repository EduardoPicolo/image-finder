from debian_image_finder.extensions.serialization import ma
from debian_image_finder.models.user import User


class UserSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = User
        load_instance = True


user_schema = UserSchema(exclude=('id',))
users_schema = UserSchema(exclude=('id',), many=True)
